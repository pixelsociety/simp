<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pixelsociety');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g4Rk|Q`}N2%|N7(rzb@2O1siAP}ErHz2]%<BYVPB/N+(MdiKw6RA=FE}8TOJsxh)');
define('SECURE_AUTH_KEY',  'F[:u=apXjfD<.*KDNQ!gkNO0D7YWVFOKm<7-tu.gRg3>KJ,tDe.:c0d3!k|u09EQ');
define('LOGGED_IN_KEY',    'c_jR:9iP(zG+/41X]nk(8LnP<TXPdf d%CM;th84NmaAx,OSKLCn:S`?35<a})-F');
define('NONCE_KEY',        'o?@<mUJ[:++C2 RJN>b8VhP5/o>EW,j5H8%x2<ZvG ,I1)t<Uf$W:*&*v?12*}@Z');
define('AUTH_SALT',        'LAQ~2u+OE(R037<rCz]$mpJ6|!-;C.jc<SL1mTqM-4ECf{l~e0M-k%tu#wXA$%yx');
define('SECURE_AUTH_SALT', 'cr&dfyjb|0_D1~7*q6J[nBTYa@8Ry)+|`2sn_wHi(>$KDfyo}1?[6^H_aoNzjFrw');
define('LOGGED_IN_SALT',   '@dq^JcQ,Wg:3:19*}o~:M)t7 Xu9{ML[{ry50Z/CS/AW/Ee>I`l-y8b?uU+/,RWS');
define('NONCE_SALT',       '|)/je:8Y%(8W8 q6`4T)h-J[qOR7NAwA|gD+V-8$v8)()zkbeOe%ey2F@$(|2nrI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'simp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
